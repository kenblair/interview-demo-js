interview-demo-js
=================

Javascript based coding examples for interviewing purposes.  Each example is in a separate directory and with tests
that can be run using npm such as `npm run sumIt`, use `npm run` to get a list.  The tests utilize a function in a
file which candidates should implement or fill in to get the tests to pass.

## SumIt

Calculate the sum of two integers that are passed as arguments.  Can be completed a number of ways usually involving a
for loop.  Resulting code commonly results in follow up questions:

- What happens if start > end?
- What might happen if both numbers are very large?
- What happens if a number is negative?
- What if something other than an integer is passed?

## Operations

Trivial examples for working with Arrays or Collections (Map, Set).

## FizzBuzz

Implement a function that returns "Fizz" when a number is divisible by 3, "Buzz" when it is divisible by 5 and "FizzBuzz"
when it is divisible by both 3 and 5.  Numbers that are not divisible by either 3 or 5 are returned as a String.

## Parentheses Balance

Implement a function that returns true when parentheses ('{}', '[]', '()') are balanced in pairs with the correct order.

## Stock Ticker

An Express based server that provides a list of stocks with a name, ticker and value.  At `/stocks` a typical REST API is
provided for CRUD.  At `/ticker` those stocks have their values changed by a random amount and the result is returned
including their new price (labeled `value`) and the change (labeled `delta`).

The server can be run using `npm run server` and will listen on port 3000.  This can either be used to test Express/back-end
knowledge by adding to or changing the API or implementing a persistent database store.  It can also be used in other tests
such as AngularJS to provide a simple server to connect to.

## Lodash

Convert an array of items into a map using the `key` and `value` properties of each item.  There are several ways to accomplish
this with Lodash such as using keyBy and mapValues, or fromPair, or even using reduce.

## AngularJS

A skeleton Angular 1.5.7 application with Bootstrap and boiler plate for two controllers in a Bootstrap grid.  The `app.js`
comes with a factory/service for connecting to the Stock Ticker server on port 3000.  The page starts with a simple list of
stocks pulled from the Stock Ticker back-end.
 
Suggested tasks:

- Turn the stock list into a Bootstrap styled table
- Convert the stock list into a directive
- Create a form for adding a new stock (just need `name` and `ticker`)
- 'Fix' the stock list so that it auto updates when a stock is added
- Write a stock banner that pulls from `getTicker` and includes the delta
- Color the stock banners depending on the delta (e.g. green for +, red for -)
- Make the stock banner auto refresh every 3 seconds


## Angular2

A skeleton Angular 2 (actually version 4+) project using TypeScript, BrowserSync and SystemJS.  First run the Stock Ticker
application on port 3000 and then run `npm start` from the `angular2` directory.  The application will be launched with
live reload through the browser.

The application itself uses a `stock-ticker` root component and module.  A `Stock` class has been provided that matches the
model expected by the stock ticker back end along with a `StockService` for communicating with the API.  There is a
`StockListComponent` with a trivial implementation that lists stock names which is useful for checking that the application
 is running correctly before beginning work.
 
Boilerplate for a `StockFormComponent` for creating a form to add a stock and a `StockBannerComponent` for utilizing the 
`/tickers` API which changes the stock prices and returns a delta.

Suggested tasks:

- Update the stock list template to use a Bootstrap styled table with ticker and price
- Convert the stock list component to retrieve stocks itself instead of taking them as an @Input
- Create a form for adding a new stock
- 'Fix' the stock list so that it updates when a new stock is added
- Write a stock banner that uses `StockService.getTickers()` to display ticker, price and delta
- Improve the styling of the stock banner: a negative delta is red, a positive delta is green, 0 delta is unchanged
- Add a button to refresh the stock banner and/or an automatic update every 3 seconds