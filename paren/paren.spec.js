'use strict';

require('chai').should();

describe('Parentheses Balance', function () {

    const paren = require('./paren');

    it('should return true if the parentheses are balanched', function () {
        paren('[()]{}{[()()]()}').should.be.true;
    });

    it('should return false if the parentheses are not balanched', function () {
        paren('[(])').should.be.false;
    });
});