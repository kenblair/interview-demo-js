'use strict';

/**
 * Return a String based on the following rules:
 *
 * If the number is not divisible by 3 or 5 return the number as a String.
 * If the number is divisible by 3 return "Fizz".
 * If the number is divisible by 5 return "Buzz".
 * If the number is divisible by 3 and 5 return "FizzBuzz".
 *
 *
 * @param num The number to return a String for.
 * @returns {*} The String.
 */
module.exports = function fizzBuzz(num) {
    return {};
};