'use strict';

require('chai').should();

describe('Fizz Buzz', function () {

    const fizzBuzz = require('./fizzbuzz');

    it('should print numbers that are not divisible by 3 or 5', function () {
        for (let i = 0; i < 100; i++) {
            if (i % 3 != 0 && i % 5 != 0) {
                fizzBuzz(i).should.equal(i.toString());
            }
        }
    });

    it('should print Fizz for numbers divisible by 3', function () {
        for (let i = 0; i < 100; i++) {
            if (i % 3 == 0 && i % 5 != 0) {
                fizzBuzz(i).should.equal('Fizz');
            }
        }
    });

    it('should print Buzz for numbers divisible by 5', function () {
        for (let i = 0; i < 100; i++) {
            if (i % 3 != 0 && i % 5 == 0) {
                fizzBuzz(i).should.equal('Buzz');
            }
        }
    });

    it('should print FizzBuzz for numbers divisible by 3 and 5', function () {
        for (let i = 0; i < 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                fizzBuzz(i).should.equal('FizzBuzz');
            }
        }
    });
});