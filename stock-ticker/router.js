'use strict';

const router = require('express').Router();

module.exports = function buildStockRouter(repository) {

    function modifyStockPrice(stock) {
        if (Math.random() < .2) {
            return 0;
        }
        let delta = Math.round(Math.random() * 1000) / 100;
        if (Math.random() < .4) {
            delta = delta * -1
        }
        stock.value = Math.round((stock.value + delta) * 100) / 100;
        return delta;
    }

    router.route('/stocks')
        .get((req, res) => {
            res.send(repository.getStocks());
        })
        .post((req, res) => {
            res.send(repository.createStock(req.body));
        });

    router.route('/stocks/:id')
        .get((req, res) => {
            res.send(repository.getStockById(req.params.id));
        })
        .post((req, res) => {
            res.send(repository.updateStock(req.body));
        })
        .delete((req, res) => {
            res.send(repository.deleteStock(req.params.id));
        });

    router.get('/ticker', (req, res) => {
        const stocks = [];
        repository.getStocks().forEach((stock) => {
            const delta = modifyStockPrice(stock);
            stocks.push({
                ticker: stock.ticker,
                value: stock.value,
                delta: delta
            });
            repository.updateStock(stock);
        });
        res.send(stocks);
    });

    return router;
};