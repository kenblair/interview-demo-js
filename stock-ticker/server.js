'use strict';

// express setup
const express = require('express')();
express.use(require('body-parser').json());
express.use(require('morgan')('dev'));
express.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
});

// build repo using initial stocks, inject repo into router, pass router back to express
express.use(require('./router')(require('./repository')(require('./initialStocks'))));

// generic error handler
express.use((err, req, res, next) => {
    res.status(500).send({error: err.message, stack: err.stack});
});

// start express
express.listen(3000, function () {
    console.log('Listening on port 3000');
});