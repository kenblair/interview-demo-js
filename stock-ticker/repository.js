'use strict';

const assert = require('assert');

module.exports = function buildStockRepository(initialStocks) {

    const stocks = {};

    let nextId = 1;

    if (initialStocks) {
        for (let i = 0; i < initialStocks.length; i++) {
            const stock = initialStocks[i];
            stock.id = nextId++;
            stocks[stock.id] = stock;
        }
    }

    return {
        getStocks: () => {
            const result = [];

            for (const id in stocks) {
                if (stocks.hasOwnProperty(id)) {
                    result.push(stocks[id]);
                }
            }

            return result;
        },
        getStockById: (id) => {
            assert(id, 'id is required');

            if (stocks.hasOwnProperty(id)) {
                return stocks[id];
            }

            return undefined;
        },
        createStock: (stock) => {
            assert(stock, 'stock is required');

            for (const id in stocks) {
                if (stocks.hasOwnProperty(id)) {
                    if (stocks[id].ticker === stock.ticker) {
                        throw new Error('duplicate stock ticker');
                    }
                }
            }

            if (!stock.hasOwnProperty('value')) {
                stock.value = Math.round(Math.random() * 1000) / 100;
            }

            stock.id = nextId++;
            stocks[stock.id] = stock;

            return stocks[stock.id];
        },
        updateStock: (stock) => {
            assert(stock, 'stock is required');
            assert(stock['id'], 'id is required');

            stocks[stock.id] = {
                id: stock.id,
                name: stock.name,
                ticker: stock.ticker,
                value: stock.value
            };

            return stocks[stock.id];
        },
        deleteStock: (id) => {
            assert(id, 'id is required');

            if (stocks.hasOwnProperty(id)) {
                const stock = stocks[id];
                delete stocks[id];
                return stock;
            }

            return false;
        }
    }
};