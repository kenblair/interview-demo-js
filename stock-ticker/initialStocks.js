// initial stocks with random starting values
module.exports = [
    {
        name: 'Burns Industries',
        ticker: 'BRN',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Brawndo Incorporated',
        ticker: 'DOH',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Aperture Science, Inc.',
        ticker: 'APT',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Lacuna Inc.',
        ticker: 'LAC',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Monsters, Inc.',
        ticker: 'MON',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Cyberdyne Systems',
        ticker: 'CYB',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Los Pollos Hermanos',
        ticker: 'LPH',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Umbrella Corporation',
        ticker: 'BAD',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Pied Piper',
        ticker: 'PIP',
        value: Math.round(Math.random() * 10000) / 100
    },
    {
        name: 'Planet Express, Inc.',
        ticker: 'PEX',
        value: Math.round(Math.random() * 10000) / 100
    }
];