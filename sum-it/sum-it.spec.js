'use strict';

require('chai').should();

describe('Sum It', function () {

    const sumIt = require('./sum-it');

    it('should calculate the sum', function () {
        sumIt(3,5).should.equal(12);
        sumIt(4,7).should.equal(22);
        sumIt(3,9).should.equal(42);
    });

});