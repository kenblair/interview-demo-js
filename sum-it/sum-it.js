'use strict';

/**
 * Calculate the sum of integers start through end inclusive.
 *
 * If 3 and 5 are passed in the result should be 12: (3 + 4 + 5) = 12.
 * If 4 and 7 are passed in the result should be 22: (4 + 5 + 6 + 7) = 22.
 *
 * @param start The start value, inclusive.
 * @param end The end value, inclusive.
 */
module.exports = function sumIt(start, end) {
    return {};
};