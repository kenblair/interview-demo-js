'use strict';

require('chai').should();

describe('Closure', function () {

  let closure = require('./closure');

  it('should give the correct response', function () {

    closure().should.have.members(['My Value: 0', 'My Value: 1', 'My Value: 2']);
  });
});