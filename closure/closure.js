'use strict';

module.exports = function closure() {
  
  var funcs = [];
  var results = [];

  for (var i = 0; i < 3; i++) {
    funcs[i] = function () {
      return "My Value: " + i;
    };
  }

  for (var j = 0; j < 3; j++) {
    results.push(funcs[j]());
  }

  return results;
};
