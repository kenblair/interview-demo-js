import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { StockTickerModule } from './app/stock-ticker.module';

platformBrowserDynamic().bootstrapModule(StockTickerModule);
