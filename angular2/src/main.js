"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var stock_ticker_module_1 = require("./app/stock-ticker.module");
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(stock_ticker_module_1.StockTickerModule);
//# sourceMappingURL=main.js.map