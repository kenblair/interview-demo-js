"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var StockService = (function () {
    function StockService(http) {
        this.http = http;
        this.baseUrl = 'http://localhost:3000';
    }
    StockService.prototype.getStocks = function () {
        return this.handle(this.http.get(this.forPath('/stocks')).toPromise());
    };
    StockService.prototype.getTickers = function () {
        return this.handle(this.http.get(this.forPath('/ticker')).toPromise());
    };
    StockService.prototype.addStock = function (stock) {
        return this.handle(this.http.post(this.forPath('/stocks'), stock).toPromise());
    };
    StockService.prototype.updateStock = function (stock) {
        return this.handle(this.http.post(this.forPath('/stocks/' + stock.id), stock).toPromise());
    };
    StockService.prototype.deleteStock = function (stock) {
        return this.handle(this.http.delete(this.forPath('/stocks/' + stock.id)).toPromise());
    };
    StockService.prototype.forPath = function (path) {
        return this.baseUrl + path;
    };
    StockService.prototype.handle = function (promise) {
        return promise.then(function (response) { return response.json(); }).catch(this.error);
    };
    StockService.prototype.error = function (error) {
        console.error(error);
        return Promise.reject(error.message || error);
    };
    return StockService;
}());
StockService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], StockService);
exports.StockService = StockService;
//# sourceMappingURL=stock.service.js.map