export class Stock {
    id: number;
    name: string;
    ticker: string;
    value: number;
    delta: number;
}