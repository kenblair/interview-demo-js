import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Stock } from './stock';

@Injectable()
export class StockService {
    private baseUrl: string = 'http://localhost:3000';

    constructor(private http: Http) {
    }

    getStocks(): Promise<Stock[]> {
        return this.handle(this.http.get(this.forPath('/stocks')).toPromise());
    }

    getTickers(): Promise<Stock[]> {
        return this.handle(this.http.get(this.forPath('/ticker')).toPromise());
    }

    addStock(stock: Stock): Promise<Stock> {
        return this.handle(this.http.post(this.forPath('/stocks'), stock).toPromise());
    }

    updateStock(stock: Stock): Promise<Stock> {
        return this.handle(this.http.post(this.forPath('/stocks/' + stock.id), stock).toPromise());
    }

    deleteStock(stock: Stock): Promise<Stock> {
        return this.handle(this.http.delete(this.forPath('/stocks/' + stock.id)).toPromise());
    }

    private forPath(path) {
        return this.baseUrl + path;
    }

    private handle(promise) {
        return promise.then(response => response.json() as Stock).catch(this.error);
    }

    private error(error: any): Promise<any> {
        console.error(error);
        return Promise.reject(error.message || error);
    }
}