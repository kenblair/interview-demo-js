import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'

import { StockTickerComponent } from './stock-ticker.component';
import { StockListComponent } from './stock-list/stock-list.component';
import { StockBannerComponent } from './stock-banner/stock-banner.component';
import { StockFormComponent } from './stock-form/stock-form.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        StockTickerComponent,
        StockListComponent,
        StockBannerComponent,
        StockFormComponent
    ],
    bootstrap: [
        StockTickerComponent
    ]
})
export class StockTickerModule {
}
