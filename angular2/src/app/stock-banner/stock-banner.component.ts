import { Component, OnInit } from '@angular/core';
import { StockService } from '../stock/stock.service';
import { Stock } from '../stock/stock';

@Component({
    moduleId: module.id,
    selector: 'stock-banner',
    templateUrl: 'stock-banner.component.html',
    providers: [StockService]
})
export class StockBannerComponent {
    constructor(private stockService: StockService) {
    }
}