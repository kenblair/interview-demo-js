import { Component } from '@angular/core';

import { StockService } from '../stock/stock.service';
import { Stock } from '../stock/stock';

@Component({
    moduleId: module.id,
    selector: 'stock-form',
    templateUrl: 'stock-form.component.html',
    providers: [StockService]
})
export class StockFormComponent {
    constructor(private stockService: StockService) {
    }


}