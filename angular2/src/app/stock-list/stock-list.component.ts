import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'stock-list',
    templateUrl: 'stock-list.component.html'
})
export class StockListComponent {
    @Input() stocks;
}