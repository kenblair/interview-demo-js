"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var forms_2 = require("@angular/forms");
var stock_ticker_component_1 = require("./stock-ticker.component");
var stock_list_component_1 = require("./stock-list/stock-list.component");
var stock_banner_component_1 = require("./stock-banner/stock-banner.component");
var stock_form_component_1 = require("./stock-form/stock-form.component");
var StockTickerModule = (function () {
    function StockTickerModule() {
    }
    return StockTickerModule;
}());
StockTickerModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            forms_1.FormsModule,
            forms_2.ReactiveFormsModule
        ],
        declarations: [
            stock_ticker_component_1.StockTickerComponent,
            stock_list_component_1.StockListComponent,
            stock_banner_component_1.StockBannerComponent,
            stock_form_component_1.StockFormComponent
        ],
        bootstrap: [
            stock_ticker_component_1.StockTickerComponent
        ]
    })
], StockTickerModule);
exports.StockTickerModule = StockTickerModule;
//# sourceMappingURL=stock-ticker.module.js.map