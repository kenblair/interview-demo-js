import { Component, OnInit } from '@angular/core';

import { StockService } from './stock/stock.service';
import { Stock } from './stock/stock';

@Component({
    moduleId: module.id,
    selector: 'stock-ticker',
    templateUrl: 'stock-ticker.component.html',
    providers: [StockService]
})
export class StockTickerComponent implements OnInit {
    title: string = 'Stock Ticker';
    stocks: Stock[];

    constructor(private stockService: StockService) {
    }

    ngOnInit(): void {
        this.getStocks();
    }

    getStocks(): void {
        this.stockService.getStocks().then(stocks => this.stocks = stocks);
    }

    getTickers(): void {
        this.stockService.getTickers().then(tickers => this.stocks = tickers);
    }
}
