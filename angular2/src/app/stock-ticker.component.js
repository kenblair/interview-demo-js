"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var stock_service_1 = require("./stock/stock.service");
var StockTickerComponent = (function () {
    function StockTickerComponent(stockService) {
        this.stockService = stockService;
        this.title = 'Stock Ticker';
    }
    StockTickerComponent.prototype.ngOnInit = function () {
        this.getStocks();
    };
    StockTickerComponent.prototype.getStocks = function () {
        var _this = this;
        this.stockService.getStocks().then(function (stocks) { return _this.stocks = stocks; });
    };
    StockTickerComponent.prototype.getTickers = function () {
        var _this = this;
        this.stockService.getTickers().then(function (tickers) { return _this.stocks = tickers; });
    };
    return StockTickerComponent;
}());
StockTickerComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'stock-ticker',
        templateUrl: 'stock-ticker.component.html',
        providers: [stock_service_1.StockService]
    }),
    __metadata("design:paramtypes", [stock_service_1.StockService])
], StockTickerComponent);
exports.StockTickerComponent = StockTickerComponent;
//# sourceMappingURL=stock-ticker.component.js.map