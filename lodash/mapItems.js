'use strict';

const _ = require('lodash');

/**
 * Convert the array of items into a map using the 'key' property of each item.
 *
 * @param items The items to map.
 * @returns {{}} A map using the key property from each item.
 */
module.exports = function mapItems(items) {
    return {};
};