'use strict';

require('chai').should();

describe('Map Values', function () {

    const mapItems = require('./mapItems');

    it('should map values by key', function () {
        const items = [{key: 'foo', value: 'bar'}, {key: 'a', value: 'b'}, {key: 123, value: 456}];
        const map = mapItems(items);
        map.should.have.property('foo', items[0]);
        map.should.have.property('a', items[1]);
        map.should.have.property('123', items[2]);
    });

});