'use strict';

const app = angular.module('stockTicker', []);

app.factory('stockRepository', ['$http', function ($http) {

    const baseUrl = 'http://localhost:3000/';

    return {
        getStocks: ()=>$http.get(baseUrl + 'stocks')
            .then((res) => {
                return res.data;
            }),
        getStock: id=>$http.get(baseUrl + 'stocks/' + id)
            .then((res) => {
                return res.data;
            }),
        getTicker: ()=>$http.get(baseUrl + 'ticker')
            .then((res) => {
                return res.data;
            }),
        addStock: stock=>$http.post(baseUrl + 'stocks', stock)
            .then((res) => {
                return res.data;
            }),
        updateStock: stock=>$http.post(baseUrl + 'stocks/' + stock.id, stock)
            .then((res) => {
                return res.data;
            }),
        removeStock: id=>$http.delete(baseUrl + 'stocks/' + id)
            .then((res) => {
                return res.data;
            })
    };
}]);


app.controller('stockListController', ['$scope', 'stockRepository', function ($scope, stockRepository) {

    stockRepository.getStocks()
        .then((stocks) => {
            $scope.stocks = stocks;
        });

}]);

app.controller('stockBannerController', ['$scope', 'stockRepository', function ($scope, stockRepository) {

}]);