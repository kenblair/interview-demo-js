'use strict';

require('chai').should();

describe('Union', function () {

    const union = require('./union');

    it('should provide the union of two arrays', function () {
        let a = ['a','b','c','d','e'];
        let b = ['b','d', 'f', 'g', 'h'];
        union(a,b).should.have.members(['a','b','c','d','e','f','g','h']);
        union(a,b).should.have.lengthOf(8);
    });

});