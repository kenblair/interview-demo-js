'use strict';

require('chai').should();

describe('Intersection', function () {

    const intersection = require('./intersection');

    it('should provide the intersection of two arrays', function () {
        let a = ['a','b','c','d','e'];
        let b = ['b','d', 'f', 'g', 'h'];
        intersection(a, b).should.have.members(['b','d']);
        intersection(a, b).should.have.lengthOf(2);
    });
});